import shopify
import flask
import json
from flask import request, jsonify
from flask_cors import CORS
import requests
from requests.auth import HTTPBasicAuth
from dotenv import load_dotenv
import boto3
import botocore.exceptions
import os
import base64
load_dotenv()

application = flask.Flask(__name__)
CORS(application, resources={r"/*": {"origins": "*"}})
application.config["DEBUG"] = True

# Shopify endpoints credentials
USERNAME = os.getenv("SHOPIFY_USERNAME")
SECRET = os.getenv("SHOPIFY_PASSWORD")
STOREFRONT_TOKEN = os.getenv("STOREFRONT_TOKEN")
STOREFRONT_URL = os.getenv("STOREFRONT_URL")
S3_ACCESS_KEY = 'AKIAIMX63FVRUNL6BTSA'
S3_SECRET_KEY = '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'
AWS_BUCKET_NAME = '2bb-braille'
SHOP_URL = 'https://%s:%s@%s/admin/api/%s' % (USERNAME, SECRET, STOREFRONT_URL, '2021-04')

shopify.ShopifyResource.set_site(SHOP_URL)
shopify.ShopifyResource._threadlocal.headers = {
    'User-Agent': 'ShopifyPythonAPI/4.0.0 Python/3.6.7',
    'X-Shopify-Access-Token': SECRET
}

client = shopify.GraphQL()

@application.route('/', methods=['GET'])
def home():
    return "<h1>Distant Reading Archive</h1><p>This site is a prototype API for distant reading of science fiction novels.</p>"

@application.route('/api/v1/upload', methods=['POST'])
def upload():
    if 'image' in request.form:
        brailleimage = base64.b64decode(request.form['image'])
        fh = open("tempimage.png", "wb")
        fh.write(request.form['image'].decode('base64'))
        fh.close()
        filename = base64.b64encode(os.urandom(6)).decode('ascii')
        s3 = boto3.client('s3', region_name='us-east-1', aws_access_key_id=S3_ACCESS_KEY, aws_secret_access_key=S3_SECRET_KEY)
        # s3 = boto3.resource('s3')
        s3.upload_file("tempimage.png", '2bb-braille', filename + '.png', ExtraArgs={
            'ACL': 'public-read',
            'ContentType' : 'image/png',
            'ContentDisposition' : 'inline; filename=' + filename + '.png'
        })
        return 'https://2bb-braille.s3.amazonaws.com/' + filename + '.png'
        # s3.Bucket('2bb-braille').put_object(Key='random_generated_name.png', Body=dec,ContentType='image/png',ACL='public-read')
    else:
        return 'error'

# @application.route('/api/v1/login', methods=['GET'])
# def login():
#     url = "https://two-blind-brothers.myshopify.com/api/2021-04/graphql"
#     login_email = request.args['email']
#     login_pass = request.args['password']
#     login_object = {
#         "input": {
#             "email": request.args['email'],
#             "password": request.args['password']
#         }
#     }
#     access_token_mutation = '''mutation {
#         customerAccessTokenCreate(email: %s, password: %s) {
#             customerAccessToken {
#                 accessToken
#                 expiresAt
#             }
#             customerUserErrors {
#                 code
#                 field
#                 message
#             }
#         }
#     }''' % (login_email, login_pass)
#
#
#     headers = {
#         "X-Shopify-Access-Token": STOREFRONT_TOKEN,
#         "Content-Type": "application/graphql",
#         "Accept": "application/json"
#         }
#     try:
#         res = requests.get(url, json={"query": access_token_mutation}, headers=headers)
#         return res.content
#     except requests.exceptions.RequestException as e:  # This is the correct syntax
#         return e
#     esult = res.text



@application.route('/api/v1/returning_customer', methods=['GET'])
def check_customer():
    customer_email = request.args['email']
    query = '''
    {
        customers(query: "email:%s", first: 1) {
            edges {
                node {
                    id
                    state
                    ordersCount
                    firstName
                    lastName
                }
            }
        }
    }
    ''' % (customer_email)
    result = json.loads(client.execute(query))
    if result and len(result['data']['customers']['edges']):
        return jsonify(result['data']['customers']['edges'][0]['node'])
    else:
        return json.dumps(False)

@application.route('/api/v1/check_for_tag', methods=['GET'])
def check_for_tag():
    customer_email = request.args['email']
    lookup_tag = request.args['tag']
    query = '''
    {
        customers(query: "email:%s", first: 1) {
            edges {
                node {
                    id
                    tags
                }
            }
        }
    }
    ''' % (customer_email)
    result = json.loads(client.execute(query))
    if result and len(result['data']['customers']['edges']):
        if lookup_tag in result['data']['customers']['edges'][0]['node']['tags']:
            return json.dumps(True)
        else:
            return json.dumps(False)
    else:
        return json.dumps(False)

@application.route('/api/v1/toggle_customer_tag', methods=['GET'])
def toggle_customer_tag():
    customer_email = request.args['email']
    add_tag = 'will-donate-discount'
    query = '''
    {
        customers(query: "email:%s", first: 1) {
            edges {
                node {
                    id
                    tags
                }
            }
        }
    }
    ''' % (customer_email)
    result = json.loads(client.execute(query))
    if result and len(result['data']['customers']['edges']):
        customer_id = result['data']['customers']['edges'][0]['node']['id']
        tags = result['data']['customers']['edges'][0]['node']['tags']
        if add_tag in tags:
            query = '''
            mutation tagsRemove($id: ID!, $tags: [String!]!) {
                tagsRemove(id: $id, tags: $tags) {
                    userErrors {
                        field
                        message
                    }
                    node {
                        id
                    }
                }
            }
            '''
            variables = { 'id': customer_id, 'tags': [add_tag] }
            result = json.loads(client.execute(query, variables))
            return json.dumps(False)
        else:
            query = '''
            mutation tagsAdd($id: ID!, $tags: [String!]!) {
                tagsAdd(id: $id, tags: $tags) {
                    userErrors {
                        field
                        message
                    }
                    node {
                        id
                    }
                }
            }
            '''
            variables = { 'id': customer_id, 'tags': [add_tag] }
            result = json.loads(client.execute(query, variables))
            return json.dumps(True)
    else:
        return json.dumps(False)

@application.route('/api/v1/toggle_test_order', methods=['GET'])
def toggle_test_order():
    customer_email = request.args['email']
    add_tag = 'test-order'
    query = '''
    {
        customers(query: "email:%s", first: 1) {
            edges {
                node {
                    id
                    tags
                }
            }
        }
    }
    ''' % (customer_email)
    result = json.loads(client.execute(query))
    if result and len(result['data']['customers']['edges']):
        customer_id = result['data']['customers']['edges'][0]['node']['id']
        tags = result['data']['customers']['edges'][0]['node']['tags']
        if add_tag in tags:
            query = '''
            mutation tagsRemove($id: ID!, $tags: [String!]!) {
                tagsRemove(id: $id, tags: $tags) {
                    userErrors {
                        field
                        message
                    }
                    node {
                        id
                    }
                }
            }
            '''
            variables = { 'id': customer_id, 'tags': [add_tag] }
            result = json.loads(client.execute(query, variables))
            return json.dumps(False)
        else:
            query = '''
            mutation tagsAdd($id: ID!, $tags: [String!]!) {
                tagsAdd(id: $id, tags: $tags) {
                    userErrors {
                        field
                        message
                    }
                    node {
                        id
                    }
                }
            }
            '''
            variables = { 'id': customer_id, 'tags': [add_tag] }
            result = json.loads(client.execute(query, variables))
            return json.dumps(True)
    else:
        return json.dumps(False)

@application.route('/api/v1/order', methods=['GET'])
def api_id():
    # Check if an ID was provided as part of the URL.
    # If ID is provided, assign it to a variable.
    # If no ID is provided, display an error in the browser.

    if 'id' in request.args:
        id = request.args['id']
        #ORDER DESK
        ORDER_DESK_ORDERS = 'https://app.orderdesk.me/api/v2/orders?source_id=%23' + id
        ORDERDESK_HEADERS = {
            "Accept": "application/json",
            "Content-Type": "application/json",
            "ORDERDESK-STORE-ID":os.getenv("OD_STORE"),
            "ORDERDESK-API-KEY":os.getenv("OD_API")
        }

        orders = requests.get(
            ORDER_DESK_ORDERS,
            headers=ORDERDESK_HEADERS
        ).json()
        # return orders
        if len(orders['orders']):
            orders_array = []
            for order in orders['orders']:
                if len(order['order_items']):
                    order_obj = {}
                    order_obj['products'] = []
                    if len(order['order_shipments']):
                        order_obj['shipments'] = []
                        for order_shipment in order['order_shipments']:
                            if order_shipment['tracking_number']:
                                order_tracking = {}
                                order_tracking['tracking'] = order_shipment['tracking_number']
                                order_tracking['tracking_url'] = order_shipment['tracking_url']
                                order_tracking['name'] = order['order_items'][0]['name']
                                order_obj['shipments'].append(order_tracking)
                    order_items = order['order_items']
                    for item in order_items:
                        item_id = item['code']
                        if item['metadata']['shopify_product_type'] == 'shopblind' or item['metadata']['shopify_product_type'] == 'subscription':
                            query = '''
            	            {
            	                productVariants(query: "sku:%s OR barcode:%s", first: 1) {
            	                    edges {
            	                        node {
            	                            image {
                                                transformedSrc(maxWidth: 500)
                                            }
                                            id
                                            displayName
                                            selectedOptions {
                                                name
                                                value
                                            }
                                            availableForSale
                                            title
                                            metafields(first: 2, namespace: "shop_blind") {
                                                edges {
                                                    node {
                                                        key
                                                        value
                                                    }
                                                }
                                            }
            	                            product {
                                                productType
                                                description
                                                descriptionHtml
                                                onlineStoreUrl
                                                images(first: 20) {
                                                    edges {
                                                        node {
                                                            altText
                                                            transformedSrc(maxWidth: 500)
                                                        }
                                                    }
                                                }
                                                title
                                            }
            	                        }
            	                    }
            	                }
            	            }
            	            ''' % (item_id, item_id)
                            result = json.loads(client.execute(query))
                            if result and len(result['data']['productVariants']['edges']):
                                product_obj = result['data']['productVariants']['edges'][0]['node']
                                if product_obj['product']['productType'] != '':
                                    if product_obj['metafields']['edges']:
                                        variantList = ""
                                        for metafield in product_obj['metafields']['edges']:
                                            if metafield['node']['key'] == 'variants_included':
                                                variantList = metafield['node']['value'].split("|")
                                        variantList = list(map(getVariantID, variantList))
                                        variantList = ",".join(variantList)
                                        # return variantList
                                        query = '''
                        	            {
                        	                productVariants(query: "product_id:%s", first: 5) {
                        	                    edges {
                        	                        node {
                        	                            image {
                                                            transformedSrc(maxWidth: 500)
                                                        }
                                                        id
                                                        displayName
                                                        selectedOptions {
                                                            name
                                                            value
                                                        }
                                                        availableForSale
                                                        title
                                                        metafields(first: 2, namespace: "shop_blind") {
                                                            edges {
                                                                node {
                                                                    key
                                                                    value
                                                                }
                                                            }
                                                        }
                        	                            product {
                                                            productType
                                                            description
                                                            descriptionHtml
                                                            onlineStoreUrl
                                                            images(first: 20) {
                                                                edges {
                                                                    node {
                                                                        altText
                                                                        transformedSrc(maxWidth: 500)
                                                                    }
                                                                }
                                                            }
                                                            title
                                                        }
                        	                        }
                        	                    }
                        	                }
                        	            }
                        	            ''' % (variantList)
                                        result = json.loads(client.execute(query))
                                        return result
                                    else:
                                        options = {}
                                        color = None
                                        for option in product_obj['selectedOptions']:
                                            if option['name'] == 'Color':
                                                color = option['value']
                                            if option['name'] != 'Title':
                                                options[option['name']] = option['value']
                                        product = {
                                            'product' : product_obj['product']['title'],
                                            'options' : options,
                                            'quantity' : item['quantity'],
                                            'description' : product_obj['product']['description'],
                                            'descriptionHtml' : product_obj['product']['descriptionHtml'],
                                            'id' : product_obj['id'],
                                            'productUrl' : product_obj['product']['onlineStoreUrl'],
                                            'available' : product_obj['availableForSale']
                                        }
                                        if product_obj['image']:
                                            product['image'] = product_obj['image']['transformedSrc']
                                        else:
                                            if color:
                                                if product_obj['product']['images']:
                                                    for image in product_obj['product']['images']['edges']:
                                                        if image['node']['altText'] == color:
                                                            product['image'] = image['node']['transformedSrc']
                                            if 'image' not in product and product_obj['product']['images'] and product_obj['product']['images']['edges']:
                                                product['image'] = product_obj['product']['images']['edges'][0]['node']['transformedSrc']
                                        order_obj['products'].append(product)
                    if len(order_obj['products']):
                        orders_array.append(order_obj)

            return jsonify(orders_array)
        else:
            return 'no orders'
    else:
        return "Error: No id field provided. Please specify an id."

def getVariantID(id):
    return "gid://shopify/ProductVariant/" + id.split(":")[1]

if __name__ == "__main__":
    application.run(host="0.0.0.0", port=443)
