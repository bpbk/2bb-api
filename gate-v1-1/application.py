import shopify
import flask
import json
from flask import request, jsonify, make_response, current_app
from flask_cors import CORS
import requests
from requests.auth import HTTPBasicAuth
from dotenv import load_dotenv
import boto3
import botocore.exceptions
import os
import base64
from datetime import timedelta
from functools import update_wrapper
load_dotenv()

application = flask.Flask(__name__)
CORS(application, resources={r"/*": {"origins": "*"}})
application.config["DEBUG"] = True

# Shopify endpoints credentials
USERNAME = os.getenv("SHOPIFY_USERNAME")
SECRET = os.getenv("SHOPIFY_PASSWORD")
STOREFRONT_TOKEN = os.getenv("STOREFRONT_TOKEN")
SHOPIFY_URL = os.getenv("STOREFRONT_URL")
S3_ACCESS_KEY = os.getenv("S3_ACCESS_KEY")
S3_SECRET_KEY = os.getenv("S3_SECRET_KEY")
AWS_BUCKET_NAME = '2bb-braille'
SHOP_URL = 'https://%s:%s@%s/admin/api/%s' % (USERNAME, SECRET, SHOPIFY_URL, '2022-04')

shopify.ShopifyResource.set_site(SHOP_URL)
shopify.ShopifyResource._threadlocal.headers = {
    'User-Agent': 'ShopifyPythonAPI/4.0.0 Python/3.6.7',
    'X-Shopify-Access-Token': SECRET
}

client = shopify.GraphQL()

def crossdomain(
    origin=None,
    methods=None,
    headers=None,
    expose_headers=None,
    max_age=21600,
    attach_to_all=True,
    automatic_options=True,
    credentials=False,
):
    """
    http://127.0.0.1:9292/
    https://twoblindbrothers.com/
    """
    if methods is not None:
        methods = ", ".join(sorted(x.upper() for x in methods))
    if headers is not None and not isinstance(headers, str):
        headers = ", ".join(x.upper() for x in headers)
    if expose_headers is not None and not isinstance(expose_headers, str):
        expose_headers = ", ".join(x.upper() for x in expose_headers)
    if not isinstance(origin, str):
        origin = ", ".join(origin)
    if isinstance(max_age, timedelta):
        max_age = max_age.total_seconds()

    def get_methods():
        if methods is not None:
            return methods

        options_resp = current_app.make_default_options_response()
        return options_resp.headers["allow"]

    def decorator(f):
        def wrapped_function(*args, **kwargs):
            if automatic_options and request.method == "OPTIONS":
                resp = current_app.make_default_options_response()
            else:
                resp = make_response(f(*args, **kwargs))
            if not attach_to_all and request.method != "OPTIONS":
                return resp

            h = resp.headers

            h["Access-Control-Allow-Origin"] = origin
            h["Access-Control-Allow-Methods"] = get_methods()
            h["Access-Control-Max-Age"] = str(max_age)
            if credentials:
                h["Access-Control-Allow-Credentials"] = "true"
            if headers is not None:
                h["Access-Control-Allow-Headers"] = headers
            if expose_headers is not None:
                h["Access-Control-Expose-Headers"] = expose_headers
            return resp

        f.provide_automatic_options = False
        return update_wrapper(wrapped_function, f)

    return decorator


@application.route('/', methods=['GET'])
def home():
    return "<h1>Distant Reading Archive</h1><p>This site is a prototype API for distant reading of science fiction novels.</p>"

@application.route('/api/v1/product', methods=['GET'])
@crossdomain(origin="*")
def get_product():
    product_id = request.args['product_id']

    query = '''
    {
        product(id: "gid://shopify/Product/%s") {
            handle
            metafields(namespace: "shop_blind_alt", first: 100) {
                edges {
                    node {
                        key
                        value
                    }
                }
            }
            featuredImage {
                url(transform: {maxWidth: 300})
            }
        }
    }
    ''' % (product_id)

    product = json.loads(client.execute(query))
    if product['data']:
        product_data = product['data']['product']
        product_data['options'] = dict({})
        product_data['image'] = product['data']['product']['featuredImage']['url']
        for metafield in product_data['metafields']['edges']:
            product_data['options'][metafield['node']['key']] = metafield['node']['value']
        return jsonify(product_data)
    return 'error'

@application.route('/api/v1/commit_orderdesk', methods=['POST'])
@crossdomain(origin="*")
def commit_orderdesk():
    request_data = request.get_json()
    order_id = request_data['orderId']
    variants = request_data['variants']
    ORDERDESK_HEADERS = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "ORDERDESK-STORE-ID":os.getenv("OD_STORE"),
        "ORDERDESK-API-KEY":os.getenv("OD_API")
    }

    # ORDER_DESK_PRODUCTS = 'https://app.orderdesk.me/api/v2/inventory-items?code=%s' % (skusQuery)

    # products = requests.get(
    #     ORDER_DESK_PRODUCTS,
    #     headers=ORDERDESK_HEADERS
    # ).json()

    # if products:
        # if products['data']:
            # for product in products['data']['inventory_items']:

    # return products

    if variants:
        variants_arr = []
        for variant in variants:
            variants_arr.append('gid://shopify/ProductVariant/' + variant['variant'])
            for variant_id in variant['products']:
                global_id = 'gid://shopify/ProductVariant/' + variant_id
                if global_id not in variants_arr:
                    variants_arr.append(global_id)
        if len(variants_arr):
            variants_query = '['
            for var_string in variants_arr:
                variants_query += '"' + var_string + '",'

            variants_query += ']'
            query = '''
            {
                nodes(ids: %s) {
                    ... on ProductVariant {
                        id
                        sku
                        displayName
                    }
                }
            }
            ''' % (variants_query)
            result = json.loads(client.execute(query))
            if result:
                if result['data']:
                    skusArr = []
                    shopify_skus = result['data']['nodes']
                    change_orders = []
                    for variant in variants:
                        new_product_object = dict({
                            'code': '',
                            'products' : [],
                            'swapped': False
                        })
                        new_obj_products = []
                        for sku in shopify_skus:
                            if variant['variant'] in sku['id']:
                                new_product_object['code'] = sku['sku']
                        for v_product in variant['products']:
                            for sku in shopify_skus:
                                if v_product in sku['id']:
                                    # NOTE: get title from OD if needed
                                    new_obj_products.append( dict({ 'code': sku['sku'], 'name': sku['displayName'], 'quantity': 1 }) )
                        new_product_object['products'] = new_obj_products
                        change_orders.append(new_product_object)

                    if len(change_orders):
                        ORDER_DESK_ORDERS = 'https://app.orderdesk.me/api/v2/orders?source_id=%23' + str(order_id)
                        orders = requests.get(
                            ORDER_DESK_ORDERS,
                            headers=ORDERDESK_HEADERS
                        ).json()

                        if len(orders['orders']):
                            order_obj = orders['orders'][0]
                            order_items = new_order_items = order_obj['order_items']
                            for oind, order_item in enumerate(order_items):
                                for item in range(0, order_item['quantity']):
                                    order_processed = False
                                    for index, change_order in enumerate(change_orders):
                                        if change_order['code'] == order_item['code'] and change_order['swapped'] == False and not order_processed:
                                            # orders_processed_arr.append(change_order['code'] + ' ' + order_item['code'] + ' ' + str(change_order['swapped']))
                                            change_orders[index]['swapped'] = True
                                            new_order_items[oind]['quantity'] -= 1
                                            order_processed = True
                                            for new_item in change_order['products']:
                                                new_order_items.append(new_item)
                            remove_items = []
                            for item in new_order_items:
                                if item['quantity'] == 0:
                                    remove_items.append(item['id'])
                            new_order_items[:] = [x for x in new_order_items if not x['quantity'] == 0]

                            # variants = json.loads(client.execute(query))

                            orders['orders'][0]['order_items'] = new_order_items

                            UPDATE_ORDER = 'https://app.orderdesk.me/api/v2/orders/' + orders['orders'][0]['id']

                            new_order = requests.put(
                                UPDATE_ORDER,
                                headers=ORDERDESK_HEADERS,
                                data=json.dumps(orders['orders'][0])
                            ).json()

                            if new_order:
                                if len(remove_items):
                                    for remove_item in remove_items:
                                        REMOVE_ORDER_ITEMS = 'https://app.orderdesk.me/api/v2/orders/' + orders['orders'][0]['id'] + '/order-items/' + remove_item
                                        remove_items = requests.delete(
                                            REMOVE_ORDER_ITEMS,
                                            headers=ORDERDESK_HEADERS
                                        ).json()
                            return new_order

@application.route('/api/v1/upload', methods=['POST'])
def upload():
    if 'image' in request.form:
        brailleimage = base64.b64decode(request.form['image'])
        fh = open("tempimage.png", "wb")
        fh.write(request.form['image'].decode('base64'))
        fh.close()
        filename = base64.b64encode(os.urandom(6)).decode('ascii')
        s3 = boto3.client('s3', region_name='us-east-1', aws_access_key_id=S3_ACCESS_KEY, aws_secret_access_key=S3_SECRET_KEY)
        # s3 = boto3.resource('s3')
        s3.upload_file("tempimage.png", '2bb-braille', filename + '.png', ExtraArgs={
            'ACL': 'public-read',
            'ContentType' : 'image/png',
            'ContentDisposition' : 'inline; filename=' + filename + '.png'
        })
        return 'https://2bb-braille.s3.amazonaws.com/' + filename + '.png'
        # s3.Bucket('2bb-braille').put_object(Key='random_generated_name.png', Body=dec,ContentType='image/png',ACL='public-read')
    else:
        return 'error'

# @application.route('/api/v1/login', methods=['GET'])
# def login():
#     url = "https://two-blind-brothers.myshopify.com/api/2021-04/graphql"
#     login_email = request.args['email']
#     login_pass = request.args['password']
#     login_object = {
#         "input": {
#             "email": request.args['email'],
#             "password": request.args['password']
#         }
#     }
#     # newtoken = shopify.CustomerAccessTokenCreateInput({email: 'brandon@fullmetalworkshop.com', password: 'T0ml0vesb00bs!'})
#
#     access_token_mutation = '''
#     mutation customerAccessTokenCreate {
#         customerAccessTokenCreate(input: { email: %s, password: %s }) {
#             customerAccessToken {
#                 accessToken
#                 expiresAt
#             }
#             customerUserErrors {
#                 code
#                 field
#                 message
#             }
#         }
#     }
#     ''' % (login_email, login_pass)
#
#     headers = {
#         "X-Shopify-Access-Token": STOREFRONT_TOKEN,
#         "Content-Type": "application/graphql",
#         "Accept": "application/json"
#     }
#
#     variables = { 'email': login_email, 'password': login_pass }
#     try:
#         res = requests.post(url=url, json={"query": access_token_mutation}, headers=headers)
#         # res = json.loads(client.execute(access_token_mutation, variables))
#         return str(res.status_code)
#     except requests.exceptions.RequestException as e:  # This is the correct syntax
#         return e



@application.route('/api/v1/returning_customer', methods=['GET'])
@crossdomain(origin="*")
def check_customer():
    customer_email = request.args['email']
    query = '''
    {
        customers(query: "email:%s", first: 1) {
            edges {
                node {
                    id
                    state
                    numberOfOrders
                    firstName
                    lastName
                }
            }
        }
    }
    ''' % (customer_email)
    result = json.loads(client.execute(query))
    
    if result and len(result['data']['customers']['edges']):
        return jsonify(result['data']['customers']['edges'][0]['node'])
    else:
        return json.dumps(False)

@application.route('/api/v1/check_for_tag', methods=['GET'])
def check_for_tag():
    customer_email = request.args['email']
    lookup_tag = request.args['tag']
    query = '''
    {
        customers(query: "email:%s", first: 1) {
            edges {
                node {
                    id
                    tags
                }
            }
        }
    }
    ''' % (customer_email)
    result = json.loads(client.execute(query))
    if result and len(result['data']['customers']['edges']):
        if lookup_tag in result['data']['customers']['edges'][0]['node']['tags']:
            return json.dumps(True)
        else:
            return json.dumps(False)
    else:
        return json.dumps(False)

@application.route('/api/v1/toggle_customer_tag', methods=['GET'])
def toggle_customer_tag():
    customer_email = request.args['email']
    add_tag = 'will-donate-discount'
    query = '''
    {
        customers(query: "email:%s", first: 1) {
            edges {
                node {
                    id
                    tags
                }
            }
        }
    }
    ''' % (customer_email)
    result = json.loads(client.execute(query))
    if result and len(result['data']['customers']['edges']):
        customer_id = result['data']['customers']['edges'][0]['node']['id']
        tags = result['data']['customers']['edges'][0]['node']['tags']
        if add_tag in tags:
            query = '''
            mutation tagsRemove($id: ID!, $tags: [String!]!) {
                tagsRemove(id: $id, tags: $tags) {
                    userErrors {
                        field
                        message
                    }
                    node {
                        id
                    }
                }
            }
            '''
            variables = { 'id': customer_id, 'tags': [add_tag] }
            result = json.loads(client.execute(query, variables))
            return json.dumps(False)
        else:
            query = '''
            mutation tagsAdd($id: ID!, $tags: [String!]!) {
                tagsAdd(id: $id, tags: $tags) {
                    userErrors {
                        field
                        message
                    }
                    node {
                        id
                    }
                }
            }
            '''
            variables = { 'id': customer_id, 'tags': [add_tag] }
            result = json.loads(client.execute(query, variables))
            return json.dumps(True)
    else:
        return json.dumps(False)

@application.route('/api/v1/toggle_test_order', methods=['GET'])
def toggle_test_order():
    customer_email = request.args['email']
    add_tag = 'test-order'
    query = '''
    {
        customers(query: "email:%s", first: 1) {
            edges {
                node {
                    id
                    tags
                }
            }
        }
    }
    ''' % (customer_email)
    result = json.loads(client.execute(query))
    if result and len(result['data']['customers']['edges']):
        customer_id = result['data']['customers']['edges'][0]['node']['id']
        tags = result['data']['customers']['edges'][0]['node']['tags']
        if add_tag in tags:
            query = '''
            mutation tagsRemove($id: ID!, $tags: [String!]!) {
                tagsRemove(id: $id, tags: $tags) {
                    userErrors {
                        field
                        message
                    }
                    node {
                        id
                    }
                }
            }
            '''
            variables = { 'id': customer_id, 'tags': [add_tag] }
            result = json.loads(client.execute(query, variables))
            return json.dumps(False)
        else:
            query = '''
            mutation tagsAdd($id: ID!, $tags: [String!]!) {
                tagsAdd(id: $id, tags: $tags) {
                    userErrors {
                        field
                        message
                    }
                    node {
                        id
                    }
                }
            }
            '''
            variables = { 'id': customer_id, 'tags': [add_tag] }
            result = json.loads(client.execute(query, variables))
            return json.dumps(True)
    else:
        return json.dumps(False)

@application.route('/api/v1/order', methods=['GET'])
def api_id():
    # Check if an ID was provided as part of the URL.
    # If ID is provided, assign it to a variable.
    # If no ID is provided, display an error in the browser.

    if 'id' in request.args:
        id = request.args['id']
        #ORDER DESK
        ORDER_DESK_ORDERS = 'https://app.orderdesk.me/api/v2/orders?source_id=%23' + id
        ORDERDESK_HEADERS = {
            "Accept": "application/json",
            "Content-Type": "application/json",
            "ORDERDESK-STORE-ID":os.getenv("OD_STORE"),
            "ORDERDESK-API-KEY":os.getenv("OD_API")
        }

        orders = requests.get(
            ORDER_DESK_ORDERS,
            headers=ORDERDESK_HEADERS
        ).json()
        # return orders
        if len(orders['orders']):
            orders_array = []
            for order in orders['orders']:
                if len(order['order_items']):
                    order_obj = {}
                    order_obj['products'] = []
                    if len(order['order_shipments']):
                        order_obj['shipments'] = []
                        for order_shipment in order['order_shipments']:
                            if order_shipment['tracking_number']:
                                order_tracking = {}
                                order_tracking['tracking'] = order_shipment['tracking_number']
                                order_tracking['tracking_url'] = order_shipment['tracking_url']
                                order_tracking['name'] = order['order_items'][0]['name']
                                order_obj['shipments'].append(order_tracking)
                    order_items = order['order_items']
                    for item in order_items:
                        item_id = item['code']
                        if item['metadata']['shopify_product_type'] == 'shopblind' or item['metadata']['shopify_product_type'] == 'subscription':
                            query = '''
            	            {
            	                productVariants(query: "sku:%s OR barcode:%s", first: 1) {
            	                    edges {
            	                        node {
            	                            image {
                                                transformedSrc(maxWidth: 500)
                                            }
                                            id
                                            displayName
                                            selectedOptions {
                                                name
                                                value
                                            }
                                            availableForSale
                                            title
                                            metafields(first: 2, namespace: "shop_blind") {
                                                edges {
                                                    node {
                                                        key
                                                        value
                                                    }
                                                }
                                            }
            	                            product {
                                                productType
                                                description
                                                descriptionHtml
                                                onlineStoreUrl
                                                images(first: 20) {
                                                    edges {
                                                        node {
                                                            altText
                                                            transformedSrc(maxWidth: 500)
                                                        }
                                                    }
                                                }
                                                title
                                            }
            	                        }
            	                    }
            	                }
            	            }
            	            ''' % (item_id, item_id)
                            result = json.loads(client.execute(query))
                            if result and len(result['data']['productVariants']['edges']):
                                product_obj = result['data']['productVariants']['edges'][0]['node']
                                if product_obj['product']['productType'] != '':
                                    options = {}
                                    color = None
                                    for option in product_obj['selectedOptions']:
                                        if option['name'] == 'Color':
                                            color = option['value']
                                        if option['name'] != 'Title':
                                            options[option['name']] = option['value']
                                    product = {
                                        'product' : product_obj['product']['title'],
                                        'options' : options,
                                        'quantity' : item['quantity'],
                                        'description' : product_obj['product']['description'],
                                        'descriptionHtml' : product_obj['product']['descriptionHtml'],
                                        'id' : product_obj['id'],
                                        'productUrl' : product_obj['product']['onlineStoreUrl'],
                                        'available' : product_obj['availableForSale']
                                    }
                                    if product_obj['metafields']['edges']:
                                        for metafield in product_obj['metafields']['edges']:
                                            if metafield['node']['key'] == 'variants_included':
                                                product['included'] = metafield['node']['value']
                                    if product_obj['image']:
                                        product['image'] = product_obj['image']['transformedSrc']
                                    else:
                                        if color:
                                            if product_obj['product']['images']:
                                                for image in product_obj['product']['images']['edges']:
                                                    if image['node']['altText'] == color:
                                                        product['image'] = image['node']['transformedSrc']
                                        if 'image' not in product and product_obj['product']['images'] and product_obj['product']['images']['edges']:
                                            product['image'] = product_obj['product']['images']['edges'][0]['node']['transformedSrc']
                                    order_obj['products'].append(product)
                    if len(order_obj['products']):
                        orders_array.append(order_obj)

            return jsonify(orders_array)
        else:
            return 'no orders'
    else:
        return "Error: No id field provided. Please specify an id."

if __name__ == "__main__":
    application.run(host="0.0.0.0", port=443)
